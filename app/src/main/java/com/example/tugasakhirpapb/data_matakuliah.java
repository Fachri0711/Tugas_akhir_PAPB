package com.example.tugasakhirpapb;

public class data_matakuliah {
    public String kode_mk;
    public String kelas;
    public String nama;
    public String no_ruangan;

    public void setKode_mk(String kode_mk) {
        this.kode_mk = kode_mk;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNo_ruangan(String no_ruangan) {
        this.no_ruangan = no_ruangan;
    }

    public String getKode_mk() {
        return kode_mk;
    }

    public String getKelas() {
        return kelas;
    }

    public String getNama() {
        return nama;
    }

    public String getNo_ruangan() {
        return no_ruangan;
    }
    public data_matakuliah(){
    }
    public data_matakuliah(String kode_mk, String kelas, String nama, String no_ruangan) {
        this.kode_mk = kode_mk;
        this.kelas = kelas;
        this.nama = nama;
        this.no_ruangan = no_ruangan;
    }
}
